<?php 
include("../../../wp-config.php");
	if($_POST['action']=="model2-arrange-visit" ){

	global $user_ID,$post;
	$postid=$_POST["postid"];	
	$userid=$_POST["userid"];	
	$user_credit=get_user_meta($user_ID,"user_credit",true);
	if($user_credit < "10"){
	user_log_activity("contact form C - out of credits");
		$buy_credit_button=buy_credit_button($postid);
		$message=array("buy_credit_button"=>$buy_credit_button,"msg"=>'<div class="alert alert-danger" role="alert">You don\'t have enough credits. Please add BBH Credits</div>',"remain_credit"=>$user_credit);
		print_r(json_encode($message));
		die();
	}
	
	$return="";
	$return.='<script>jQuery(function() { jQuery( "#date_selection" ).datepicker({ format: "dd/mm/yyyy" }); });</script>';
	$return.="<form action='' method='POST'>";
	$return.="<p>Great!! We will contact the owner to make sure the property still is available and we will get back to you as soon as possible</p>";
	$return.="<div id='rent_date_message'></div>";
	$return.="<label for='date_selection'>I want to rent from:</label>";
	$return.="<input type='text' id='date_selection' name='date_selection' value='' required />";
	$return.= "<label for='message_to_us'>Message To Us:</label>";
	$return.="<input type='hidden' id='postid' name='postid' value='".$postid."' />";
	$return.="<input type='hidden' id='userid' name='userid' value='".$userid."' />";
	$return.= "<textarea placeholder='What would you like us to ask about this property? If you want us to schedule an appointment, please give us dates/times that would work for you. Let us know anything you want us to ask/mention.' name='message_to_us' id='message_to_us'></textarea><br/>";
	$return.= '<div class="modal-a-buttons">';
	$return.="<input class='btn btn-primary' type='button' onclick='return send_mail_popup2()' id='user_email_submit' name='user_email_submit' value='Send'/>";
	$return.= '</div>';
	$return.="</form>";
	$message=array("msg"=>$return);
	print_r(json_encode($message));
	die();
	
}
if($_POST['action']=="arrange_communications" ){

	global $user_ID,$post;
	$postid=$_POST["postid"];	
	$userid=$_POST["userid"];	
	$user_credit=get_user_meta($user_ID,"user_credit",true);
	if($user_credit < "2"){
	user_log_activity("contact form B - out of credits");
		$buy_credit_button=buy_credit_button($postid);
		$message=array("buy_credit_button"=>$buy_credit_button,"msg"=>'<div class="alert alert-danger" role="alert">You don\'t have enough credits. Please add BBH Credits</div>',"remain_credit"=>$user_credit);
		print_r(json_encode($message));
		die();
	}
	
	$return="";
	$return.='<script>jQuery(function() { jQuery( "#date_selection" ).datepicker({ format: "dd/mm/yyyy" }); });</script>';
	$return.="<form action='' method='POST'>";
	$return.="<p>Great!! We will contact the owner to make sure the property still is available and we will get back to you as soon as possible</p>";
	$return.="<div id='rent_date_message'></div>";
	$return.="<label for='date_selection'>I want to rent from:</label>";
	$return.="<input type='text' id='date_selection' name='date_selection' value='' required />";
	$return.= "<label for='message_to_us'>Message To Us:</label>";
	$return.="<input type='hidden' id='postid' name='postid' value='".$postid."' />";
	$return.="<input type='hidden' id='userid' name='userid' value='".$userid."' />";
	$return.= "<textarea placeholder='What would you like us to ask about this property? If you want us to schedule an appointment, please give us dates/times that would work for you. Let us know anything you want us to ask/mention.' name='communication_feedback' id='communication_feedback'></textarea><br/>";
	$return.= '<div class="modal-a-buttons">';
	$return.="<input class='btn btn-primary' type='button' onclick='return send_mail_for_communication()' name='mail_for_communication' value='Send'/>";
	$return.= '</div>';
	$return.="</form>";
	$message=array("msg"=>$return);
	print_r(json_encode($message));
	die();
	
}

if($_POST['action']=="sendmailfor-arrange-visit" ){
		
	$postid=$_POST["postid"];	
	$userid=$_POST["userid"];	
	$text=$_POST["text"];		
	$user_credit=get_user_meta($userid,"user_credit",true);

	if($user_credit < "10"){
	$buy_credit_button=buy_credit_button($postid);
		$message=array("buy_credit_button"=>$buy_credit_button,"msg"=>'<div class="alert alert-danger" role="alert">You don\'t have enough credits. Please add BBH Credits</div>',"remain_credit"=>$user_credit);
		print_r(json_encode($message));
		die();
	}
	$user_credit=$user_credit-10;
    update_user_meta ($userid, 'user_credit', $user_credit);
	$user_credit=get_user_meta($userid,"user_credit",true);

    $template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=1");
    $extradata = array('user_feedback' => $text, 'rent_date_from' => $_POST['rent_date_from']);
    $messagebody = parse_template($template_message->template_message, $userid, $postid, $extradata);
    	
	$user_info = get_userdata($userid);
	$user_firstname=$user_info->first_name;
	$user_lastname=$user_info->last_name;
	$user_facebook_url=$user_info->user_url;
	$owner_name = get_post_meta( $postid, 'owner_name',true ); 			
	$owner_email = get_post_meta( $postid, 'owner_email',true ); 	
	$owner_phone = get_post_meta( $postid, 'owner_phone',true ); 	
	$postlink=get_permalink( $postid);
	$from="admin@balibudgethousing.com";
	$to="hello@balibudgethousing.com";
	//$to='alpesh.s.php@gmail.com';
	//$subject="VISIT INQUIYRY for BBH".$postid;
	$subject="INQ-VISIT-BBH".$postid."-".$user_firstname.$user_lastname;
	/*$message="Hello,<br/>
	User First Name:".$user_firstname."<br/>
	User Last Name:".$user_lastname."<br/>
	User Facebook: ".$user_facebook_url."<br/>
	User-Email = ".$user_info->user_email."<br/>	
	Owner-Name = ".$owner_name."<br/>
	Owner-Phone = ".$owner_phone."<br/>
	Listing = <a href='".$postlink."'>BBH".$postid."</a><br/>
	I want to rent from=".$_POST['rent_date_from']."<br/>
	User-Message = ".$text."<br/>
	";*/	
	$message = $messagebody;
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	mail( 't.a.huges@gmail.com', $subject, $message,$headers); 
	user_log_activity("Request contact info from Option C");      
	//SEND MAIL TO USER
	sendmailtouser($userid);
	$message=array("msg"=>'<div class="alert alert-success" role="alert">Thank You For Your Inquiry, We Will get back to you soon!!!</div>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();
}
if($_POST['action']=="send_mail_for_communication" ){
		
	$postid=$_POST["postid"];	
	$userid=$_POST["userid"];	
	$text=$_POST["text"];		
	$user_credit=get_user_meta($userid,"user_credit",true);

	if($user_credit < "2"){
		$buy_credit_button=buy_credit_button($postid);
		$message=array("buy_credit_button"=>$buy_credit_button,"msg"=>'<div class="alert alert-danger" role="alert">You don\'t have enough credits. Please add BBH Credits</div>',"remain_credit"=>$user_credit);
		print_r(json_encode($message));
		die();
	}
	$user_credit=$user_credit-2;
    update_user_meta ($userid, 'user_credit', $user_credit);
	$user_credit=get_user_meta($userid,"user_credit",true);

	
	$user_info = get_userdata($userid);
	$user_firstname=$user_info->first_name;
	$user_lastname=$user_info->last_name;
	$user_facebook_url=$user_info->user_url;
	$owner_name = get_post_meta( $postid, 'owner_name',true ); 			
	$owner_email = get_post_meta( $postid, 'owner_email',true ); 	
	$owner_phone = get_post_meta( $postid, 'owner_phone',true ); 	
	$postlink=get_permalink( $postid);
	
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=2");
   	$extradata = array('user_feedback' => $text, 'rent_date_from' => $_POST['rent_date_from']);
	$messagebody = parse_template($template_message->template_message, $userid, $postid, $extradata);
	
	$from="admin@balibudgethousing.com";
	$to="hello@balibudgethousing.com";
	//$to="alpesh.s.php@gmail.com";
	//$to=$owner_email;
	//$subject="VISIT INQUIYRY for BBH".$postid;
	$subject="INQ-COMMUNICATE-BBH".$postid."-".$user_firstname.$user_lastname;
	/*$message="Hello,<br/>
	User First Name:".$user_firstname."<br/>
	User Last Name:".$user_lastname."<br/>
	User Facebook: ".$user_facebook_url."<br/>
	User-Email = ".$user_info->user_email."<br/>	
	Owner-Name = ".$owner_name."<br/>
	Owner-Phone = ".$owner_phone."<br/>
	Listing = <a href='".$postlink."'>BBH".$postid."</a><br/>
	I want to rent from=".$_POST['rent_date_from']."<br/>
	User-Message = ".$text."<br/>
	";*/	
	$message = $messagebody;
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	mail( 't.a.huges@gmail.com', $subject, $message,$headers); 
       user_log_activity("Request contact info from Option B");
	
	//SEND MAIL TO USER
	sendmailtouser($userid);
	$message=array("msg"=>'<div class="alert alert-success" role="alert">Thank You For Your Inquiry, We Will get back to you soon!!!</div>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();
}

if($_POST['action']=="contact_owner" ){

	$userID=$_POST["userid"];
	$userDATA=get_userdata( $userID );
	$useremail=$userDATA->user_email;
	
	
	$postid=$_POST['postid'];
	$posts = get_post($postid);
	$owner_name=get_post_meta($postid,"owner_name",true);
	$owner_phone=get_post_meta($postid,"owner_phone",true);
	$owner_email=get_post_meta($postid,"owner_email",true);
	$owner_contactinfo=get_post_meta($postid,"address",true);
	$phonenumber=checkownerphone($owner_phone);
	
	//credit -1
	$user_credit=get_user_meta($userID,"user_credit",true);
	if($user_credit <= "0"){
		user_log_activity("contact form A - out of credits");
		$buy_credit_button=buy_credit_button($postid);
		$message=array("buy_credit_button"=>$buy_credit_button,"msg"=>'<div class="alert alert-danger" role="alert">You don\'t have enough credits. Please add BBH Credits</div>',"remain_credit"=>$user_credit);
		print_r(json_encode($message));
		die();
	}
	$user_credit=$user_credit-1;
    update_user_meta ($userID, 'user_credit', $user_credit);
	$user_credit=get_user_meta($userID,"user_credit",true);
    $datefrm = date("d M Y",strtotime($_POST['renting_this_property_from']));
	$extradata = array('renting_this_property_from' => $datefrm, 'my_home_country_is' => $_POST['my_home_country_is']);
	//send email to user
	$from="hello@balibudgethousing.com";
	$to=$useremail;
			//$to='trivedi.s.php@gmail.com';
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=3");
	$usermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);					
	$subject="Contact info for BBH".$postid;		
								   
	$subject="Contact info for BBH".$postid;
	/*$message="<html><body><p>you can contact ".$owner_name." of <a  href='".get_permalink($postid)."'>BBH".$postid."</a> at:</p>
	<p>Phone : ".$owner_phone."</p>";
	if(!empty($owner_email)){
	$message.="<p>Email: ".$owner_email."</p>";
	}
	else{
	$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
	}
	$message.="<p>Let us know how it turned out and good luck finding your perfect spot in beautiful Bali!</p>
	<p>Cheers,<br/>
	the Bali Budget Housing team</p></body></html>";	
	*/
	$message="<html><body>".$usermessagebody."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	//mail( 'mohsin.k.php@gmail.com', $subject, $message,$headers); 
	
	
	//send mail to hello@balibudgethousing.com
	
	$fromadmin="admin@balibudgethousing.com";
	
	
	//$to="alpesh.s.php@gmail.com";
	$tobali="hello@balibudgethousing.com";							   						   //$tobali="trivedi.s.php@gmail.com";							   					
	$subject="CONTACT INFO SENT for BBH".$postid;
	/*$message="<html><body>
	<p>BBHID=BBH".$postid."</p>
	<p>user-email=".$useremail."</p>
	<p>you can contact ".$owner_name." of <a  href='".get_permalink($postid)."'>BBH".$postid."</a> at:</p>
	<p>Phone : ".$owner_phone."</p>
	<p>Email: ".$owner_email."</p>
	<p>Let us know how it turned out and good luck finding your perfect spot in beautiful Bali!</p>
	<p>Cheers,<br/>
	the Bali Budget Housing team</p></body></html>";	
	*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=5");
	$adminmessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
    
	$message="<html><body>".$adminmessagebody."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$fromadmin.'>' . "\r\n";
	//mail( $tobali, $subject, $message,$headers); 
	mail( 'hello@balibudgethousing.com', $subject, $message,$headers);
	mail( 'yennysuryadi@gmail.com', $subject, $message,$headers);
	//mail( 'mohsin.k.php@gmail.com', $subject, $message,$headers);
	if(!empty($owner_email)){
		//sent email to owner
		
		$fromowner="hello@balibudgethousing.com";
		$ownerto=$owner_email;
		//$ownerto="trivedi.s.php@gmail.com";
		//$ownerto="hello@balibudgethousing.com";
		$subject="Inquiry for your property";
		/*$message="<html><body>Hello,<br/>
		<p>a user on our website requested your contact info for this listing:<a  href='".get_permalink($postid)."'>BBH".$postid."</a>.</p>
		<p>We hope you will come to a deal you are both happy with!</p>
		<p>All the best,<br/>the Bali Budget Housing team<p>
		<p>P.S. If your accommodation already is not available anymore,please let us know and we will remove it so you wont get any requests for it anymore.<p>
		</body></html>";*/
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=4");
	        $ownermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);
		$message="<html><body>".$ownermessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers);
		//mail('mohsin.k.php@gmail.com', $subject, $message,$headers);
	}
	else{
		
		if(!empty($owner_phone)){
			//sent message to owner
			$phonenumber=checkownerphone($owner_phone);
			/*require('twilio-php-master/Services/Twilio.php'); 

				$account_sid = 'ACd2730e2f6d2f8ae37434ab7a2c56cc58'; // ACcf8ef5802eeb577e07b0b8dee13d3e87, ACb2e67a525d4c065fb58f659d78905bec

				$auth_token = '988a0330833da733a493456eb89f3071'; // 1e3287ba9a187b33330ba7c1ba52056e,6ce1d3d9eb0bd764ea1a033a623690d9
				$client = new Services_Twilio($account_sid, $auth_token); 
				 //+6289637993630
				 //SMS#1:from balibudgethousing.com: we've sent your contact info to a user interested in your property BBH".$postid.".Hope you'll come to a deal !
				//SMS#2:let us know if it's not available anymore and we'll remove it from the website so you wont get any requests anymore.
				$call=$client->account->messages->create(array( 
					'To' => $phonenumber, 
					'From' => "+17864204066",    
					'Body' => "Dari balibudgethousing.com: Kami sudah mengirim kontak info anda ke user kami yang tertarik dengan property anda BBH".$postid.". Semoga sukses.",   
				));
				//+15005550006
				$call1=$client->account->messages->create(array( 
					'To' => $phonenumber, 
					'From' => "+17864204066",    
					'Body' => "Tlg info kami jika properti anda sdh tdk disewakan lagi. Kami akan menghapusnya dari website shg anda tdk akan mendptkan permintaan yg sama dr kami.",   
				));
				*/
				send_store_sms($phonenumber,$postid);

		}
		else{
			
			
		$fromowner="admin@balibudgethousing.com";
		
		$ownerto="hello@balibudgethousing.com";
		
		
		
		$subject="VISIT INQUIRY - for BBH".$postid."-can't reach owner to send sms/email notification";
		/*$message="<html><body>
		<p>BBHID=BBH".$postid."</p>
		<p>user-email=".$useremail."</p>";
		if(!empty($owner_email)){
			$message.="<p>Email: ".$owner_email."</p>";
			}
			else{
			$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
			}
		$message.="<p>owner-name=".$owner_name."</p>
		<p>owner-contactinfo=".$owner_contactinfo."</p>
		</body></html>";*/
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=6");
	        $inquirymessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
	        $message="<html><body>".$inquirymessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		//mail( $ownerto, $subject, $message,$headers);  
			mail( 'hello@balibudgethousing.com', $subject, $message,$headers);
			mail( 'yennysuryadi@gmail.com', $subject, $message,$headers);
			//mail( 'mohsin.k.php@gmail.com', $subject, $message,$headers);
		}
		
		
	}
	followup_email_queued_msg($postid);
	user_log_activity("contact form A - contact info sent");
	$message=array("msg"=>'<div class="alert alert-success" role="alert">We\'ve sent the contact info to your email.</div>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();
}

if($_POST['action']=="share_message_send_contact" ){

	$userID=$_POST["userid"];
	$userDATA=get_userdata( $userID );
	$useremail=$userDATA->user_email;
	
	$firstname=$userDATA->first_name;
	$lastname=$userDATA->last_name;
	$user_website=$userDATA->user_url;
	$user_gender=get_user_meta($userID,"facebook_user_gender",true);
	$user_locale=get_user_meta($userID,"facebook_user_locale",true);
	$user_timezone=get_user_meta($userID,"facebook_user_timezone",true);
	
	
	
	$postid=$_POST['postid'];
	$posts = get_post($postid);
	$owner_name=get_post_meta($postid,"owner_name",true);
	$owner_phone=get_post_meta($postid,"owner_phone",true);
	$owner_email=get_post_meta($postid,"owner_email",true);
	$owner_contactinfo=get_post_meta($postid,"address",true);
	$phonenumber=checkownerphone($owner_phone);
	insertFBClicked($userID,$postid);
	//credit -1
	$user_credit=get_user_meta($userID,"user_credit",true);
$datefrm = date("d M Y",strtotime($_POST['renting_this_property_from']));
$extradata = array('renting_this_property_from' => $datefrm, 'my_home_country_is' => $_POST['my_home_country_is']);
	//send email to user
	$from="hello@balibudgethousing.com";
	$to=$useremail;
	//$to="trivedi.s.php@gmail.com";							   
	$subject="Contact info for BBH".$postid;
	/*$message="<html><body><p>you can contact ".$owner_name." of <a  href='".get_permalink($postid)."'>BBH".$postid."</a> at:</p>
	<p>Phone : ".$owner_phone."</p>";
	if(!empty($owner_email)){
	$message.="<p>Email: ".$owner_email."</p>";
	}
	else{
	$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
	}
	$message.="<p>Let us know how it turned out and good luck finding your perfect spot in beautiful Bali!</p>
	<p>Cheers,<br/>
	the Bali Budget Housing team</p></body></html>";
	*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=3");
    $usermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);		
	$message="<html><body>".$usermessagebody."</body></html>";	
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	
	
	//send mail to hello@balibudgethousing.com
	
	$fromadmin="admin@balibudgethousing.com";
	$tobali="hello@balibudgethousing.com";							   
//$tobali="trivedi.s.php@gmail.com";
	$subject="CONTACT INFO SENT for BBH".$postid;
	/*$message="<html><body>
	<p>BBHID=BBH".$postid."</p>
	<p>user-email=".$useremail."</p>
	<p>you can contact ".$owner_name." of <a  href='".get_permalink($postid)."'>BBH".$postid."</a> at:</p>
	<p>Phone : ".$owner_phone."</p>
	<p>Email: ".$owner_email."</p>
	<p>Let us know how it turned out and good luck finding your perfect spot in beautiful Bali!</p>
	<p>Cheers,<br/>
	the Bali Budget Housing team</p></body></html>";	
	*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=5");
    $adminmessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
    
    $message="<html><body>".$adminmessagebody."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$fromadmin.'>' . "\r\n";
	mail( $tobali, $subject, $message,$headers); 
	
	//sent email for SHARED TO FACEBOOK
	
	$sharefacebookfrom="admin@balibudgethousing.com";
	$helloto="hello@balibudgethousing.com";
	//$helloto="trivedi.s.php@gmail.com";
	$subject="A-SHARED TO FACEBOOK";
	/*$message="<html><body>Hello,<br/>
	<p>Method:Facebook authentication</p>
	<p>User First Name:".$firstname."</p>
	<p>User Last Name:".$lastname."</p>
	<p>User E-Mail: ".$useremail."</p>
	<p>User Facebook profile: ".$user_website."</p>
	<p>User Gender:".$user_gender."</p>
	<p>User Locale:".$user_locale."</p>
	<p>User Timezone:".$user_timezone."</p>
	<p>User Credits:".$user_credit."<p>
	<p>Listing:<a  href='".get_permalink($postid)."'>BBH".$postid."</a></p>
	</body></html>";*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=7");
	$sharedtofacebookbody= parse_template($template_message->template_message, $userID, $postid,$extradata);
    	$message="<html><body>".$sharedtofacebookbody."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$sharefacebookfrom.'>' . "\r\n";
	mail( $helloto, $subject, $message,$headers); 
	
	if(!empty($owner_email)){
		//sent email to owner
		
		$fromowner="hello@balibudgethousing.com";
		$ownerto=$owner_email;
		//$ownerto="trivedi.s.php@gmail.com";
		$subject="Inquiry for your property";
		/*$message="<html><body>Hello,<br/>
		<p>a user on our website requested your contact info for this listing:<a  href='".get_permalink($postid)."'>BBH".$postid."</a>.</p>
		<p>We hope you will come to a deal you are both happy with!</p>
		<p>All the best,<br/>the Bali Budget Housing team<p>
		<p>P.S. If your accommodation already is not available anymore,please let us know and we will remove it so you wont get any requests for it anymore.<p>
		</body></html>";
		*/
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=4");
	        $ownermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);
		$message="<html><body>".$ownermessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers); 
	}
	else{
		
		if(!empty($owner_phone)){
			//sent message to owner
			$phonenumber=checkownerphone($owner_phone);
			send_store_sms($phonenumber,$postid);

		/*	require('twilio-php-master/Services/Twilio.php'); 


				$account_sid = 'ACd2730e2f6d2f8ae37434ab7a2c56cc58'; // ACcf8ef5802eeb577e07b0b8dee13d3e87, ACb2e67a525d4c065fb58f659d78905bec

				$auth_token = '988a0330833da733a493456eb89f3071'; // 1e3287ba9a187b33330ba7c1ba52056e,6ce1d3d9eb0bd764ea1a033a623690d9
				$client = new Services_Twilio($account_sid, $auth_token); 
				 //+6289637993630
				 //SMS#1:from balibudgethousing.com: we've sent your contact info to a user interested in your property BBH".$postid.".Hope you'll come to a deal !
				//SMS#2:let us know if it's not available anymore and we'll remove it from the website so you wont get any requests anymore.
				$call=$client->account->messages->create(array( 
					'To' => $phonenumber, 
					'From' => "+17864204066",    
					'Body' => "Dari balibudgethousing.com: Kami sudah mengirim kontak info anda ke user kami yang tertarik dengan property anda BBH".$postid.". Semoga sukses.",   
				));
				//+15005550006
				$call1=$client->account->messages->create(array( 
					'To' => $phonenumber, 
					'From' => "+17864204066",    
					'Body' => "Tlg info kami jika properti anda sdh tdk disewakan lagi. Kami akan menghapusnya dari website shg anda tdk akan mendptkan permintaan yg sama dr kami.",   
				));
				*/
				
		}
		else{
			
			
		$fromowner="admin@balibudgethousing.com";
		$ownerto="hello@balibudgethousing.com";
		//$ownerto="trivedi.s.php@gmail.com";
		
		
		$subject="VISIT INQUIRY - for BBH".$postid."-can't reach owner to send sms/email notification";
		/*$message="<html><body>
		<p>BBHID=BBH".$postid."</p>
		<p>user-email=".$useremail."</p>";
		if(!empty($owner_email)){
			$message.="<p>Email: ".$owner_email."</p>";
			}
			else{
			$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
			}
		$message.="<p>owner-name=".$owner_name."</p>
		<p>owner-contactinfo=".$owner_contactinfo."</p>
		</body></html>";*/
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=6");
	        $inquirymessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
	        $message="<html><body>".$inquirymessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers); 
			
		}
		
		
	}
	followup_email_queued_msg($postid);
	user_log_activity("contact form A - sent contact info after sharing with facebook");
	$message=array("msg"=>'<div class="alert alert-success" role="alert">The message was shared on your Facebook timeline and we\'ve sent the contact info to your email.</div>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();
}
if($_POST['action']=="share_message_after_sending_contact_info" ){

	$userID=$_POST["userid"];
	$userDATA=get_userdata( $userID );
	$useremail=$userDATA->user_email;
	
	//credit -1
	$user_credit=get_user_meta($userID,"user_credit",true);
	$user_credit=$user_credit+1;
    update_user_meta ($userID, 'user_credit', $user_credit);
	$user_credit=get_user_meta($userID,"user_credit",true);

	$message=array("msg"=>'<span style="color: green;font-weight: bold;">The message was shared on your Facebook timeline.</span>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();
}
if(isset($_POST["action"]) && $_POST["action"]=="resend_mail_user" ){
	global $wpdb,$posts,$user_ID;
	$userDATA=get_userdata( $user_ID );
	$useremail=$userDATA->user_email;
	
	$postid=$_POST['postid'];
	//$feedback=$_POST['feedback'];
	$owner_name=get_post_meta($postid,"owner_name",true);
	$owner_phone=get_post_meta($postid,"owner_phone",true);
	$owner_email=get_post_meta($postid,"owner_email",true);
	$owner_contactinfo=get_post_meta($postid,"address",true);
	
	$from="hello@balibudgethousing.com";
	$to=$useremail;
								   
	$subject="Contact info for BBH".$postid;
	/*$message="<html><body><p>you can contact ".$owner_name." of <a  href='".get_permalink($postid)."'>BBH".$postid."</a> at:</p>
	<p>Phone : ".$owner_phone."</p>";
	if(!empty($owner_email)){
	$message.="<p>Email: ".$owner_email."</p>";
	}
	else{
	$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
	}
	$message.="<p>Let us know how it turned out and good luck finding your perfect spot in beautiful Bali!</p>
	<p>Cheers,<br/>
	the Bali Budget Housing team</p></body></html>";	
	*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=3");
	$usermessagebody = parse_template($template_message->template_message, $userID, $postid);		
	$message="<html><body>".$usermessagebody."</body></html>";
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	user_log_activity("resent contact info");
	echo '<div class="alert alert-success" role="alert">We\'ve resent the email!</div>';
	
}
if(isset($_POST["action"]) && $_POST["action"]=="share_to_twitter" ){
	$userID=$_POST['userid'];
	$postid=$_POST['postid'];
	$userID=$_POST['userid'];
	$postid=$_POST['postid'];
insertTwitterClicked($userID,$postid);
	$userDATA=get_userdata( $user_ID );
	$useremail=$userDATA->user_email;
	$firstname=$userDATA->first_name;
	$lastname=$userDATA->last_name;
	$user_website=$userDATA->user_url;
	$user_gender=get_user_meta($userID,"facebook_user_gender",true);
	$user_locale=get_user_meta($userID,"facebook_user_locale",true);
	$user_timezone=get_user_meta($userID,"facebook_user_timezone",true);
	
	
	
	$postid=$_POST['postid'];
	$posts = get_post($postid);
	$owner_name=get_post_meta($postid,"owner_name",true);
	$owner_phone=get_post_meta($postid,"owner_phone",true);
	$owner_email=get_post_meta($postid,"owner_email",true);
	$owner_contactinfo=get_post_meta($postid,"address",true);
	$phonenumber=checkownerphone($owner_phone);
	
	//credit -1
	$user_credit=get_user_meta($userID,"user_credit",true);
$datefrm = date("d M Y",strtotime($_POST['renting_this_property_from']));
$extradata = array('renting_this_property_from' => $datefrm, 'my_home_country_is' => $_POST['my_home_country_is']);
	//send email to user
	$from="hello@balibudgethousing.com";
	$to=$useremail;
			//$to='trivedi.s.php@gmail.com';					   
	$subject="Contact info for BBH".$postid;
	
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=3");
    $usermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);		
	$message="<html><body>".$usermessagebody."</body></html>";	
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$from.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	
	
	//send mail to hello@balibudgethousing.com
	
	$fromadmin="admin@balibudgethousing.com";
	//$to="alpesh.s.php@gmail.com";
	$tobali="hello@balibudgethousing.com";							   
       //$tobali="trivedi.s.php@gmail.com";	
       $subject="CONTACT INFO SENT for BBH".$postid;
	
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=5");
    $adminmessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
    
    $message="<html><body>".$adminmessagebody."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$fromadmin.'>' . "\r\n";
	mail( $tobali, $subject, $message,$headers); 
	
	//sent email for SHARED TO Twitter

	$sharefacebookfrom="admin@balibudgethousing.com";
	$helloto="hello@balibudgethousing.com";
	//$helloto="trivedi.s.php@gmail.com";
	$subject="A-SHARED TO TWITTER";
	
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=7");
	$sharedtofacebookbody= parse_template($template_message->template_message, $userID, $postid,$extradata);

	$message="<html><body>".$sharedtofacebookbody."</body></html>";
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$sharefacebookfrom.'>' . "\r\n";
	mail( $helloto, $subject, $message,$headers); 
	
	if(!empty($owner_email)){
		//sent email to owner
		
		$fromowner="hello@balibudgethousing.com";
		$ownerto=$owner_email;
		//$ownerto="trivedi.s.php@gmail.com";
		$subject="Inquiry for your property";
		
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=4");
	        $ownermessagebody = parse_template($template_message->template_message, $userID, $postid,$extradata);
		$message="<html><body>".$ownermessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers); 
	}
	else{
		
		if(!empty($owner_phone)){
			//sent message to owner
			$phonenumber=checkownerphone($owner_phone);
			send_store_sms($phonenumber,$postid);

		}
		else{
			
			
		$fromowner="admin@balibudgethousing.com";
		$ownerto="hello@balibudgethousing.com";
		//$ownerto="alpesh.s.php@gmail.com";
		
		
		$subject="VISIT INQUIRY - for BBH".$postid."-can't reach owner to send sms/email notification";
		
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=6");
	        $inquirymessagebody= parse_template($template_message->template_message, $userID, $postid,$extradata);
	        $message="<html><body>".$inquirymessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers); 
			
		}
		
		
	}
	followup_email_queued_msg($postid);
	user_log_activity("contact form A - sent contact info after sharing with twitter");
	$message=array("msg"=>'<div class="alert alert-success" role="alert">The message was shared on your Twitter timeline and we\'ve sent the contact info to your email.</div>',"remain_credit"=>$user_credit);
	print_r(json_encode($message));
	die();

}
function insertTwitterClicked($user_id,$postid){
	global $wpdb;
	$userhasclicked=$wpdb->get_row("SELECT * FROM wp_bbh_user_clicked_twitter_button WHERE user_id=$user_id");
	if(!empty($userhasclicked)){
		$updateclickedData=array('clicked_datetime'=>current_time('mysql'));
		$wpdb->UPDATE('wp_bbh_user_clicked_twitter_button',$updateclickedData,array('id'=>$userhasclicked->id));
	}else{
		$insertclickeddata=array('user_id'=>$user_id,'postid'=>$postid,'clicked_datetime'=>current_time('mysql'));
		$wpdb->INSERT('wp_bbh_user_clicked_twitter_button',$insertclickeddata);
	}
}
function insertFBClicked($user_id,$postid){
	global $wpdb;
	$userhasclicked=$wpdb->get_row("SELECT * FROM wp_bbh_user_clicked_fb_button WHERE user_id=$user_id");
	if(!empty($userhasclicked)){
		$updateclickedData=array('clicked_datetime'=>current_time('mysql'));
		$wpdb->UPDATE('wp_bbh_user_clicked_fb_button',$updateclickedData,array('id'=>$userhasclicked->id));
	}else{
		$insertclickeddata=array('user_id'=>$user_id,'postid'=>$postid,'clicked_datetime'=>current_time('mysql'));
		$wpdb->INSERT('wp_bbh_user_clicked_fb_button',$insertclickeddata);
	}
}
function sendmailtouser($userid){
 global $wpdb;
 
	$user_info = get_userdata($userid);
	$user_firstname=$user_info->first_name;
	$user_email=$user_info->user_email;
	$fromuser="hello@balibudgethousing.com";
	$to=$user_email;
	//$to="alpesh.s.php@gmail.com";
	$subject="Request confirmed";
	/*$message="<html><body>Hi ".$user_firstname.",<br/>
	<p>we've received your request and we'll get back to you as soon as possible.</p>
	<p>The Bali Budget Housing Team.<br/>
		P.S. Haven't liked us on Facebook yet? <a href='https://www.facebook.com/balibudgethousing'>Please do!</a>
	</p></body></html>";	
	*/
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=8");
        $requestconfirmed = parse_template($template_message->template_message, $userid, 0);		
	$message="<html><body>".$requestconfirmed."</body></html>";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<'.$fromuser.'>' . "\r\n";
	mail( $to, $subject, $message,$headers); 
	user_log_activity("contact form B or C - Request confirmed");
	 
}
function checkownerphone($owner_phone){

	if(is_array(explode(",",$owner_phone))){
		$first_number=explode(",",$owner_phone);
		$owner_phone=$first_number[0];
	}	
	$number = str_replace(" ", "", $owner_phone); // remove space 
	$str = ltrim($number, '0');// remove first 0
	//adding +62 country code
	$phonenumber="+62".$str;
	return $phonenumber;
}
function buy_credit_button($postid){
	global $wpdb;
	
	$link=get_permalink($postid);		
	/*$buy_credit_button="<div class='bbh_credit'>Buy BBH credits:".do_shortcode('[paypal_button type="paynow" id="3175" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/10_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3176" amount="25" name="30 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/30_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3177" amount="100" name="50 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/50_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."</div>";*/
	/*$buy_credit_button="<div class='bbh_credit'>Buy BBH credits:".do_shortcode('[paypal_button type="paynow" id="3175" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/10_BBH_credits.png" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3176" amount="25" name="30 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/30_BBH_credits.png" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3177" amount="100" name="50 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/50_BBH_credits.png" returnmethod="1"]')."<img src='http://www.balibudgethousing.com/wp-content/uploads/2014/10/paypal-verified.png' /></div>";*/
	$buy_credit_button="<div class='bbh_credit'>Buy BBH credits:".do_shortcode('[paypal_button type="paynow" id="3175" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/10_BBH_credits.png" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3176" amount="25" name="30 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/30_BBH_credits.png" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3177" amount="100" name="50 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/50_BBH_credits.png" returnmethod="1"]')."</div><div class='bbh_paypal'style='padding-top:20px;'><strong>After succesfull payment, please WAIT until PayPal brings you back to our website, or the purchase might not register properly.</strong><p>&nbsp;</p><img src='http://www.balibudgethousing.com/wp-content/uploads/2014/10/paypal-verified.png' /></div>";
	return $buy_credit_button;
}

function check_time($t1, $t2, $tn) {
    $t1 = +str_replace(":", "", $t1);
    $t2 = +str_replace(":", "", $t2);
    $tn = +str_replace(":", "", $tn);
    if ($t2 >= $t1) {
        return $t1 <= $tn && $tn < $t2;
    } else {
        return ! ($t2 <= $tn && $tn < $t1);
    }
}
function send_store_sms($owner_phone_number,$postid){
global $wpdb,$user_ID;
	//$sms1="Dari balibudgethousing.com: Kami sudah mengirim kontak info anda ke user kami yang tertarik dengan property anda BBH".$postid.". Semoga sukses.";
	//$sms2="Tlg info kami jika properti anda sdh tdk disewakan lagi. Kami akan menghapusnya dari website shg anda tdk akan mendptkan permintaan yg sama dr kami.";
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=12");
    $sms1 = parse_template($template_message->template_message, $user_ID,$postid);	
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=13");
    $sms2 = parse_template($template_message->template_message, $user_ID,$postid);	
	//$owner_phone_number="+6289637993630";
	//$current_time = date('H:i:s');
	$current_time=current_time("H:i:s");
	//$current_time = date('H:i:s',mktime(date('H')+12,date('i'),date('s')));
	
	$comparitiontime = array(   
		array("08:00:00", "19:00:00", $current_time)
	);
	foreach($comparitiontime as $test) {
		list($t1, $t2, $t0) = $test;
		  $midnight_or =  (check_time($t1, $t2, $t0) ? "no" : "yes");		
	}

	if($midnight_or=="yes"){
	
	//store sms in database
		$insertarray=array(
			'owner_phone'=>$owner_phone_number,
			'sms1'=>$sms1,
			'sms2'=>$sms2
		);
		
		$wpdb->INSERT("wp_bbh_queue_owner_sms",$insertarray);
		
		
	}else{
	if(!checkPhoneNumber($owner_phone_number)){
	//send message to owner
	require('twilio-php-master/Services/Twilio.php'); 
     	$account_sid = 'ACd2730e2f6d2f8ae37434ab7a2c56cc58';
		$auth_token = '988a0330833da733a493456eb89f3071'; 
		$client = new Services_Twilio($account_sid, $auth_token); 
		$call=$client->account->messages->create(array( 
				'To' => $owner_phone_number, 
				'From' => "+17864204066",    
				'Body' => $sms1,   
			));
			$call1=$client->account->messages->create(array( 
				'To' => $owner_phone_number, 
				'From' => "+17864204066",    
				'Body' => $sms2,   
			));
	
	}else{
		send_mail_if_owner_phone_is_not_valid($user_ID,$postid);
	}
	
	}
	
}
function send_mail_if_owner_phone_is_not_valid($userid,$postid){
global $wpdb;
	$fromowner="admin@balibudgethousing.com";
		$ownerto="hello@balibudgethousing.com";
		//$ownerto="alpesh.s.php@gmail.com";
		
		
		$subject="VISIT INQUIRY - for BBH".$postid."-can't reach owner to send sms/email notification";
		/*$message="<html><body>
		<p>BBHID=BBH".$postid."</p>
		<p>user-email=".$useremail."</p>";
		if(!empty($owner_email)){
			$message.="<p>Email: ".$owner_email."</p>";
			}
			else{
			$message.="<p>Email:-we unfortunately don't have email contact info for this listing-</p>";
			}
		$message.="<p>owner-name=".$owner_name."</p>
		<p>owner-contactinfo=".$owner_contactinfo."</p>
		</body></html>";*/
		$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=6");
	        $inquirymessagebody= parse_template($template_message->template_message, $userid, $postid);
	        $message="<html><body>".$inquirymessagebody."</body></html>";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
		mail( $ownerto, $subject, $message,$headers); 
		//mail( "alpesh.s.php@gmail.com", $subject, $message,$headers); 
}
function followup_email_queued_msg($postid=0){
	global $wpdb,$user_ID;
	
	$currenttime=date("Y-m-d");
	$log_user_exits=$wpdb->get_row("select * from wp_bbh_followup_email_msg_queue WHERE user_id=$user_ID and postid=$postid and status=0");
	if(!empty($log_user_exits)){
		$update_array=array(
			'datetime'=>$currenttime
		);
		$wpdb->UPDATE("wp_bbh_followup_email_msg_queue",$update_array,array("id"=>$log_user_exits->id));
		
	}else{
		$insert_array=array(
			'user_id'=>$user_ID,
                        'postid'=>$postid, 
			'datetime'=>$currenttime
		);
		$wpdb->INSERT("wp_bbh_followup_email_msg_queue",$insert_array);
	}
	
}
function parse_template($content, $user_id, $postid, $extradata = array()) {
    global $wpdb;

    $userDATA = get_userdata($user_id);
    $useremail = $userDATA->user_email;
    $firstname = $userDATA->first_name;
    $lastname = $userDATA->last_name;
    $user_website = $userDATA->user_url;
    $user_gender = get_user_meta($user_id, "facebook_user_gender", true);
    $user_locale = get_user_meta($user_id, "facebook_user_locale", true);
    $user_timezone = get_user_meta($user_id, "facebook_user_timezone", true);
    $user_credit = get_user_meta($user_id, "user_credit", true);

    $postid = $_POST['postid'];
    $posts = get_post($postid);
    $owner_name = get_post_meta($postid, "owner_name", true);
    $owner_phone = get_post_meta($postid, "owner_phone", true);
    $owner_email = get_post_meta($postid, "owner_email", true);
    $owner_contactinfo = get_post_meta($postid, "address", true);
    $permalink = get_permalink($postid);

    if(!empty($owner_email)){
        $owner_email=$owner_email;      
        
    }
    else{
        $owner_email='we unfortunately don\'t have email contact info for this listing-';
        
    }
	if(get_the_title( $postid )=="Payment Failed"){
		$permalink=get_permalink("3223");//bbh credit page
	}
	$area = get_post_meta($postid, "Area", true);
	$title=get_the_title($postid); 
	$content = str_replace("{area}",$area, $content);
	$content = str_replace("{title}",$title, $content);
    $content = str_replace("{user_firstname}", $firstname, $content);
    $content = str_replace("{user_lastname}", $lastname, $content);
    $content = str_replace("{user_facebook_url}", $user_website, $content);
    $content = str_replace("{user_email}", $useremail, $content);
    $content = str_replace("{user_gender}", $user_gender, $content);
    $content = str_replace("{user_locale}", $user_locale, $content);
    $content = str_replace("{user_timezone}", $user_timezone, $content);
    $content = str_replace("{owner_phone}", $owner_phone, $content);
    $content = str_replace("{owner_name}", $owner_name, $content);
    $content = str_replace("{owner_email}", $owner_email, $content);
    $content = str_replace("{owner_contactinfo}", $owner_contactinfo, $content);
    $content = str_replace("{listingid}", "<a href='" . $permalink . "'>BBH" . $postid . "</a>", $content);
    $content = str_replace("{BBHID}", "BBH" .$postid, $content);
    $content = str_replace("{rent_date_from}", $extradata['rent_date_from'], $content);
    $content = str_replace("{user_feedback}", $extradata['user_feedback'], $content);
    $content = str_replace("{user_credit}", $user_credit, $content);
	$content = str_replace("{authenticate_method}", $extradata['authenticate_method'], $content);
	$content = str_replace("{verification_link}", "<a href='".$extradata['verification_link']."'>Click here</a>", $content);
	$content = str_replace("{button_clicked}", $extradata['button_clicked'], $content);
	$content = str_replace("{active_page}", "<a href='".$permalink."'>Click here</a>", $content);
	$content = str_replace("{user_from}", $extradata['my_home_country_is'], $content);
	$content = str_replace("{rent_date}", $extradata['renting_this_property_from'], $content);

    return $content;
}
function user_log_activity($action='',$user=''){
	global $wpdb,$user_ID;	
	
	//$log_datetime=date("Y-m-d H:i:s");
	$log_datetime=current_time("mysql");
	$user_ip_address=$_SERVER['REMOTE_ADDR'];

	$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$user_ip_address));
	if($ip_data && $ip_data->geoplugin_countryName != null){
		$user_country_code = $ip_data->geoplugin_countryCode;
	}
	else{
		$user_country_code="";
	}
	
	if(empty($user)){
		$user=$user_ID;
	}
	$insert_user_log=array(
		'userid'=>$user,
		'action'=>$action,
		'log_datetime'=>$log_datetime,
		'user_country_code'=>$user_country_code,
		'user_ip'=>$user_ip_address
	);
	
	
	$userlogactivityTable=$wpdb->prefix.'user_log_activity';
	$wpdb->INSERT($userlogactivityTable,$insert_user_log);
	
	
}
if(isset($_POST['action']) && $_POST['action']=="send_mail_for_bbh_buy_clicked"){
	
	global $wpdb,$posts,$user_ID;
	//USER CLICKED BUY CREDITS
	
	$from = "admin@balibudgethousing.com";
    $to = 'hello@balibudgethousing.com';
//$to = 'alpesh.s.php@gmail.com';
    $subject = "USER CLICKED BUY CREDITS";
	$item_number=$_REQUEST['id1'];
	if ($item_number == "3175") {
		$button_clicked = "10x $10";
	}
	if ($item_number == "3176") {
		$button_clicked = "30x $25";
	}
	if ($item_number == "3177") {
		$button_clicked = "50x $100";
	}
	$postid=$_POST['postid'];
	user_log_activity("clicked Buy Now ".$button_clicked." button");
	$extraarray =array("button_clicked"=>$button_clicked);
	$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=14");
    $userclickedmessagebody = parse_template($template_message->template_message, $user_ID,$postid,$extraarray);		
	$message="<html><body>".$userclickedmessagebody."</body></html>";
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<' . $from . '>' . "\r\n";
	mail($to, $subject, $message, $headers);
	//user_log_activity("Buy BBH Credits-Payment Success");
}

function checkPhoneNumber($a){
		$result = substr($a, 0, 7);
	if (preg_match('/6236/',$result)){
		return true;
	}
	else if (preg_match('/036/',$result)){
		return true;
	}
	else if(strpos($result,"+6236") !== false){

		return true;
	}
	else{
		return false;
	}
}
if(isset($_POST['action']) && $_POST['action'] == "send_mail_for_bussiness_space"){

	$buss_user_email=$_POST['buss_user_email'];
	
	if(!empty($buss_user_email)){
		$email=$buss_user_email;
	}else{
		$email="No Email!!";
	}
	//send mail to hello@bbh.com
	$from="admin@balibudgethousing.com";
	$to="hello@balibudgethousing.com";
	//$to="alpesh.s.php@gmail.com";
	$subject="POLL-YES-BUSINESS SPACES";
	$message="<html><body>
	<p>Email: ".$email."</p>
	</body></html>";
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: Bali Budget Housing<' . $from . '>' . "\r\n";
	$sent=mail($to, $subject, $message, $headers);
	if($sent){
		echo '<div class="alert alert-success" role="alert">Thanks!!</div>';
		die();
	}
	else{
		echo '<div class="alert alert-success" role="alert">No Email Sent!!</div>';
		die();
	}
	

}

?>