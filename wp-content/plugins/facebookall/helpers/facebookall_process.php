<?php

function facebookall_make_userlogin() {
global $new_user1,$wpdb;
  $fball_settings = get_option('fball_settings');
  if (isset($_GET['code']) AND !empty($_GET['code'])) {
    $code = $_GET['code'];
    parse_str(facebookall_get_fb_contents("https://graph.facebook.com/oauth/access_token?" . 'client_id=' . $fball_settings ['apikey'] . '&redirect_uri=' . urlencode(site_url()) .'&client_secret=' .  $fball_settings ['apisecret'] . '&code=' . urlencode($code)));
   if(empty($access_token)) {
     parse_str(facebookall_get_fb_contents("https://graph.facebook.com/oauth/access_token?" . 'client_id=' . $fball_settings ['apikey'] . '&redirect_uri=' . urlencode(site_url().'/') .'&client_secret=' .  $fball_settings ['apisecret'] . '&code=' . urlencode($code)));
    }
   ?>
   <script>
         window.opener.FbAll.parentRedirect({'action' : 'fball', 'fball_access_token' : '<?php echo $access_token?>'});
         window.close();
       </script>
  <?php }
  if(!empty($_REQUEST['fball_access_token']) AND isset($_REQUEST['fball_redirect'])) {
    $fbuser_info = json_decode(facebookall_get_fb_contents("https://graph.facebook.com/me?access_token=".$_REQUEST['fball_access_token']));
    $fbdata = facebookall_get_fbuserprofile_data($fbuser_info);
   $_SESSION['popup'] ="1";
   //echo "<pre>";print_r($fbdata);exit;
    if (!empty($fbdata['email']) AND !empty($fbdata['id'])) {
      // Filter username form data.
      if(!empty($fbdata['name'])) {
        $username = $fbdata['name'];
      }
      else if (!empty($fbdata['first_name']) && !empty($fbdata['last_name'])) {
        $username = $fbdata['first_name'].$fbdata['last_name'];
      }
      else {
      $user_emailname = explode('@', $fbdata['email']);
        $username = $user_emailname[0];
      }
     $user_login = sanitize_user($username, true);
     $new_user = false;
     //$new_user1 = 0;
      $user_id = facebookall_get_userid($fbdata['id']);
      if (!is_numeric ($user_id) AND empty($user_id)) {
        if (($user_id_tmp = email_exists ($fbdata['email'])) !== false) {
          $user_data = get_userdata ($user_id_tmp);
          if ($user_data !== false) {
            $user_id = $user_data->ID;
            $user_login = $user_data->user_login;
         if (!empty ($fbdata['gender'])) {
               update_user_meta ($user_id, 'facebook_user_gender', $fbdata['gender']);
             }
          if (!empty ($fbdata['locale'])) {
               update_user_meta ($user_id, 'facebook_user_locale', $fbdata['locale']);
             }
          if (!empty ($fbdata['timezone'])) {
               update_user_meta ($user_id, 'facebook_user_timezone', $fbdata['timezone']);
             }
            if (!isset ($fball_settings ['linkaccount']) OR $fball_settings ['linkaccount'] == 'link') {
              delete_metadata ('user', null, 'facebookall_user_id', $fbdata['id'], true);
              update_user_meta ($user_id, 'facebookall_user_id', $fbdata['id']);
              update_user_meta ($user_id, 'facebookall_user_email', $fbdata['email']);
           update_user_meta ($user_id, 'new_user', "0");
           
              if (!empty ($fbdata['thumbnail'])) {
                update_user_meta ($user_id, 'facebookall_user_thumbnail', $fbdata['thumbnail']);
              }
              wp_cache_delete ($user_id, 'users');
              wp_cache_delete ($user_login, 'userlogins');
            }
          }
        }
        else {
        $new_user = true;
        $new_user1 = 1;
          $user_login = facebookall_usernameexists($user_login);
          $user_password = wp_generate_password ();
        $user_role = get_option('default_role');
          $user_data = array (
                     'user_login' => $user_login,
                     'display_name' => (!empty ($fbdata['name']) ? $fbdata['name'] : $user_login),
                     'user_email' => $fbdata['email'],
                     'first_name' => $fbdata['first_name'],
                     'last_name' => $fbdata['last_name'],
                     'user_url' => $fbdata['website'],
                     'user_pass' => $user_password,
                     'description' => $fbdata['aboutme'],
                         'role' => $user_role
                  );
           $user_id = wp_insert_user ($user_data);
           if (is_numeric ($user_id)) {
         
         add_user_meta($user_id, 'new_user', "1",true);
         $usercredit=get_option("credit");
             delete_metadata ('user', null, 'facebookall_user_id', $fbdata['id'], true);
             update_user_meta ($user_id, 'facebookall_user_id', $fbdata['id']);
             update_user_meta ($user_id, 'facebookall_user_email', $fbdata['email']);
          
         // delete_metadata ('user', null, 'user_credit', 0, true);
          add_user_meta ($user_id, 'user_credit', $usercredit,true);
             if (!empty ($fbdata['thumbnail'])) {
               update_user_meta ($user_id, 'facebookall_user_thumbnail', $fbdata['thumbnail']);
             }
          if (!empty ($fbdata['gender'])) {
               update_user_meta ($user_id, 'facebook_user_gender', $fbdata['gender']);
             }
          if (!empty ($fbdata['locale'])) {
               update_user_meta ($user_id, 'facebook_user_locale', $fbdata['locale']);
             }
          if (!empty ($fbdata['timezone'])) {
               update_user_meta ($user_id, 'facebook_user_timezone', $fbdata['timezone']);
             }
			 
			  //send mail to hello@balibudgethousing.com
			 
			 $fromowner="admin@balibudgethousing.com";
			 $to="hello@balibudgethousing.com";
			//$to="alpesh.s.php@gmail.com";
			$subject="NEW USER - ".$fbdata['first_name']." ".$fbdata['last_name']."";
			/*$message="<html><body>Hello,<br/>
			<p>Method:Facebook authentication</p>
			<p>User First Name:".$fbdata['first_name']."</p>
			<p>User Last Name:".$fbdata['last_name']."</p>
			<p>User E-Mail: ".$fbdata['email']."</p>
			<p>User Facebook profile: ".$fbdata['website']."</p>
			<p>User Gender:".$fbdata['gender']."</p>
			<p>User Locale:".$fbdata['locale']."</p>
			<p>User Timezone:".$fbdata['timezone']."</p>
			<p>User Credits:".$usercredit."<p>
			</body></html>";
			*/
			$extraarray=array("authenticate_method"=>'Facebook authentication');
			$template_message = $wpdb->get_row("SELECT * FROM wp_bbh_template_messages WHERE id=9");
			$newusermessagebody = parse_template($template_message->template_message, $user_id, 0,$extraarray);	
			$message="<html><body>".$newusermessagebody."</body></html>";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$headers .= 'From: Bali Budget Housing<'.$fromowner.'>' . "\r\n";
			mail( $to, $subject, $message,$headers); 
             wp_cache_delete ($user_id, 'users');
             wp_cache_delete ($user_login, 'userlogins');
             do_action ('user_register', $user_id);
           }
         }
       }
       $user_data = get_userdata ($user_id);
       if ($user_data !== false) {
        facebookall_post_user_wall($_REQUEST['fball_access_token'], $fbdata['id'], $new_user);
         wp_clear_auth_cookie ();
         wp_set_auth_cookie ($user_data->ID, true);
         user_log_activity("Authenticated with Facebook",$user_id);
         do_action ('wp_login', $user_data->user_login, $user_data);
       // Redirect user.
         if (!empty ($_GET['redirect_to'])) {
           $redirect_to = $_GET['redirect_to'];
           wp_safe_redirect ($redirect_to);
         }
         else {
           $redirect_to = facebookall_redirect_loggedin_user();
           wp_redirect ($redirect_to);
         }
         exit();
       }
     }
   }
 }
 if(!function_exists('parse_template')){
 function parse_template($content, $user_id, $postid, $extradata = array()) {
    global $wpdb;
	

    $userDATA = get_userdata($user_id);
    $useremail = $userDATA->user_email;
    $firstname = $userDATA->first_name;
    $lastname = $userDATA->last_name;
    $user_website = $userDATA->user_url;
    $user_gender = get_user_meta($user_id, "facebook_user_gender", true);
    $user_locale = get_user_meta($user_id, "facebook_user_locale", true);
    $user_timezone = get_user_meta($user_id, "facebook_user_timezone", true);
    $user_credit = get_user_meta($user_id, "user_credit", true);

    $postid = $_POST['postid'];
    $posts = get_post($postid);
    $owner_name = get_post_meta($postid, "owner_name", true);
    $owner_phone = get_post_meta($postid, "owner_phone", true);
    $owner_email = get_post_meta($postid, "owner_email", true);
    $owner_contactinfo = get_post_meta($postid, "address", true);
    $permalink = get_permalink($postid);

    if(!empty($owner_email)){
        $owner_email=$owner_email;      
        
    }
    else{
        $owner_email='we unfortunately don\'t have email contact info for this listing-';
        
    }
	if(get_the_title( $postid )=="Payment Failed"){
		$permalink=get_permalink("3711");//bbh credit page
	}
	$area = get_post_meta($postid, "Area", true);
	$title=get_the_title($postid); 
	$content = str_replace("{area}",$area, $content);
	$content = str_replace("{title}",$title, $content);
    $content = str_replace("{user_firstname}", $firstname, $content);
    $content = str_replace("{user_lastname}", $lastname, $content);
    $content = str_replace("{user_facebook_url}", $user_website, $content);
    $content = str_replace("{user_email}", $useremail, $content);
    $content = str_replace("{user_gender}", $user_gender, $content);
    $content = str_replace("{user_locale}", $user_locale, $content);
    $content = str_replace("{user_timezone}", $user_timezone, $content);
    $content = str_replace("{owner_phone}", $owner_phone, $content);
    $content = str_replace("{owner_name}", $owner_name, $content);
    $content = str_replace("{owner_email}", $owner_email, $content);
    $content = str_replace("{owner_contactinfo}", $owner_contactinfo, $content);
    $content = str_replace("{listingid}", "<a href='" . $permalink . "'>BBH" . $postid . "</a>", $content);
    $content = str_replace("{BBHID}", "BBH" .$postid, $content);
    $content = str_replace("{rent_date_from}", $extradata['rent_date_from'], $content);
    $content = str_replace("{user_feedback}", $extradata['user_feedback'], $content);
    $content = str_replace("{user_credit}", $user_credit, $content);
	$content = str_replace("{authenticate_method}", $extradata['authenticate_method'], $content);
	$content = str_replace("{button_clicked}", $extradata['button_clicked'], $content);
	$content = str_replace("{verification_link}", "<a href='".$extradata['verification_link']."'>Click here</a>", $content);
	$content = str_replace("{active_page}", "<a href='".$permalink."'>Click here</a>", $content);
	$content = str_replace("{user_from}", $extradata['my_home_country_is'], $content);
	$content = str_replace("{rent_date}", $extradata['renting_this_property_from'], $content);
    return $content;
}
}
if(!function_exists('user_log_activity')){
function user_log_activity($action='',$user=''){
	global $wpdb,$user_ID;	
	
	//$log_datetime=date("Y-m-d H:i:s");
	$log_datetime=current_time("mysql");
	$user_ip_address=$_SERVER['REMOTE_ADDR'];

	$ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$user_ip_address));
	if($ip_data && $ip_data->geoplugin_countryName != null){
		$user_country_code = $ip_data->geoplugin_countryCode;
	}
	else{
		$user_country_code="";
	}
	
	if(empty($user)){
		$user=$user_ID;
	}
	$insert_user_log=array(
		'userid'=>$user,
		'action'=>$action,
		'log_datetime'=>$log_datetime,
		'user_country_code'=>$user_country_code,
		'user_ip'=>$user_ip_address
	);
	
	$userlogactivityTable=$wpdb->prefix.'user_log_activity';
	$wpdb->INSERT($userlogactivityTable,$insert_user_log);
	
	
}
}
function after_login(){
   $user_ID = get_current_user_id(); 
   
   $new_user=get_user_meta($user_ID,"new_user",true);
   
   $STRING ="";
   $STRING.=display_profile();
   /*if($new_user ==1){ //new user
         $STRING.=modelpopup2();
   }*/
   //if($new_user == 0){
      $user_credit=get_user_meta($user_ID,"user_credit",true);
      /*if($user_credit > 0){
            $STRING.=modelpopup2();
      }
      if($user_credit == 0 || $user_credit == "")
      {
            $STRING.=modelpopup3();
      }*/
      
   //}
      $STRING.=modelpopup2();
    return $STRING;
   
}
function modelpopup2(){
global $post;
$postid=$post->ID;
$link=get_permalink($postid);
global $user_ID,$wpdb;
 
      ?>
      
<div class="modal fade" id="model2" style="display:none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><!--<span class="sr-only">Close</span>--></button>
        <h4 class="modal-title">Get contact info for this accommodation</h4>
      </div>
      <div class="modal-body">
     
      <div id="modelpopupbody">
      <?php if(!empty($user_ID)){
            $user_credit=get_user_meta($user_ID,"user_credit",true);
            $creditcontent=($user_credit == 1 ? "credit" : "credits");
         /*echo "<p><strong>You have <span id='user_credit'>".$user_credit."</span> ".$creditcontent."</strong>&nbsp;&nbsp;<a href='".site_url()."/bbh-credits/' target='_blank'><em>what's this?</em></a><br />".
         "Buy BBH credits: <a href='#'><strong>10x</strong>&nbsp;&nbsp;$10</a>,&nbsp;&nbsp;&nbsp;<a href='#'><strong>30x</strong>&nbsp;&nbsp;$25</a>,&nbsp;&nbsp;&nbsp;<a href='#'><strong>50x</strong>&nbsp;&nbsp;$100</a></p>";*/
       /*echo "<div class='bbh_credit'><p><strong>You have <span id='user_credit'>".$user_credit."</span> ".$creditcontent."</strong>&nbsp;&nbsp;<a href='".site_url()."/bbh-credits/' target='_blank'><em>what's this?</em></a></p>".
         "Buy BBH credits:".do_shortcode('[paypal_button type="paynow" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/10_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3176" amount="25" name="30 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/30_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."".do_shortcode('[paypal_button type="paynow" id="3177" amount="100" name="50 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/09/50_BBH_credits.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]')."</div>";*/
		echo "<p><strong>You have <span id='user_credit' class='user_credit'>".$user_credit."</span> ".$creditcontent."</strong>&nbsp;&nbsp;<a href='".site_url()."/bbh-credits/' target='_blank'><em>what's this?</em></a></p>"; 
         }
      ?>
      <input type="hidden" name="postid" id="postid" value="<?php echo $postid;?>" >
      <input type="hidden" name="userid" id="userid" value="<?php echo $user_ID;?>" >
      <p>I'm interested in renting this property from:</p>
	   <input style='z-index:220001;' type='text' id='renting_this_property_from' name='renting_this_property_from' value='' required readonly="readonly"/>
	  <p>My home country is:</p>
	  <input type='text' id='my_home_country_is' name='my_home_country_is' value='' required />	
	  <p id='errmsg'></p>
	  <br/>
      <?php
	  $fbclickedData=$wpdb->get_row("SELECT * FROM wp_bbh_user_clicked_fb_button WHERE user_id=$user_ID");
	  $clickeddate=strtotime($fbclickedData->clicked_datetime);
	  $todaydate=strtotime(current_time("mysql"));
	//  $diff = ($todaydate- $clickeddate)/12/3600;
$diff = 0; 
	?>
	<style>
		
		.tooltip-wrapper {
			display: block;
		}
		
		</style>
      
      <!--<h4><br />A - I want the contact info for this property</h4>
      
      <p class="showthis">Most contact persons (either the owner or local agents) have limited English skills and the main feedback we get is that it’s difficult to communicate. If this isn’t a problem, we’ll send you the contact info we have (usually phone number).</p>-->
  	<?php if(is_facebook_user()):?>
  	<div class="tooltiphide showbuttoninline">
	   <?php if($diff < 1 ) : ?>
	   <span class="tooltip-wrapper" data-toggle="tooltip" data-placement="top" title="You can share a message to Facebook once per day. Please use another option or wait until tomorrow."></span>
	   <?php endif;?>
	  <input class="btn btn-primary" type='button' name='share_message_to_facebook' value='Send me local contact info (share BBH on FB)' id="share_button" <?php if($diff < 1 ) { echo "disabled=disabled"; ?>  
	  <?php } ?> >
	  <div class='clear'></div>
	  </div>
	  <?php endif;?>
	   <?php
		
		$twitterClickedData=$wpdb->get_row("SELECT * FROM wp_bbh_user_clicked_twitter_button WHERE user_id=$user_ID");
		$twclickeddate=strtotime($twitterClickedData->clicked_datetime);
		$twtodaydate=strtotime(current_time("mysql"));
		$daydiff = ($twtodaydate- $twclickeddate)/12/3600;
		?>
		<div class="tooltiphidefortwitter showbuttoninline">
		<?php if($daydiff < 1 ) : ?>
	   <span class="tooltip-for-twitter" data-toggle="tooltip" data-placement="top" title="You can share a message to Twitter once per day. Please use another option or wait until tomorrow."></span>
	   <?php endif;?>
	    <?php 
		$twmessage="@BaliBhousing: Checkout balibudgethousing.com for affordable longterm rentals in #Bali";
	   ?>
		<a  style='text-decoration:none;' class="btn btn-primary" id='tweetlink' href="https://twitter.com/intent/tweet?url=http://balibudgethousing.com/&text=<?php echo urlencode($twmessage);?>" <?php if($daydiff < 1 ) { echo "disabled=disabled"; ?>  
	  <?php } ?>>Share on Twitter</a>
		</div>		
      <input class="btn btn-primary" type='button' name='contact_the_owner_myself' value='Use 1 credit' id="contact_owner">
      
      <!--<h4><br />B - Call the contact person for the property  for me</h4>
      <p class="showthis">Let us know what you want to know / find out or when you would like us to make an appointment and we'll call for you.</p>
      <input class="btn btn-primary" type='button' name='arrage_communications' value='Make inquiry for me (2 credits)' id="arrange_communications">
-->
      <!--<h4><br />C - I want visit the property and have you join me</h4>
      <p id="whatthis3" class="whatthis">&#x25BC; what's this?</p><p class="showthis" style="display:none;">Finally we can arrange a visit to the property and accompany you. We'll join you at a well known location and tell your driver where to find the property. At the property we'll take care of all necessary translations.</p>
      <input class="btn btn-primary" type='button' name='arrage_visit_to_the_house' value='Arrange for a visit and accompany me  ( 10 credits )' id="arrange_visit_house">
      -->
      <div class="modal-a-buttons">
      </div>
      
      </div>
	  <div id="responsebody"></div>
      <div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>
      </div>
      <div class="modal-footer">
        
        <!--<a href="/bbh-credits/" target="blank" style="text-decoration:none;"><button type="button" class="btn btn-primary">How does this work?</button></a>-->
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


      <script>
      jQuery(document).ready(function($){
      <?php
      if ( is_user_logged_in() ) { 
         if(isset($_SESSION['popup']) && $_SESSION['popup'] =="1"){
      ?>
         jQuery("#model2").modal("show");
		jQuery("#Tabs li a[href='#t3']").click();		 
         <?php $_SESSION['popup'] ="0"; ?>
      <?php
      }
      }
      ?>
   
   
         jQuery("#Tabs li a[href='#t3']").click(function(e){
            e.preventDefault();
            jQuery("#model2").modal("show");
			jQuery("#modelpopupbody").show(); 
			jQuery("#responsebody").html('');
			jQuery("#errmsg").html(''); 
         });
         
         

      });
         
      </script>
<?php

$STRING="";
/*    $STRING="";
      $STRING.="<input type='button' name='contact_the_owner_myself' value='I will Contact The Owner Myself'>";
      $STRING.="<input type='button' name='arrage_visit_to_the_house' value='Please arrange a visit to the house'>";
      return $STRING;*/
}
function is_facebook_user(){
	global $user_ID;
	$user=get_user_meta ($user_ID, 'facebookall_user_id', true);
	if(!empty($user))
		return true;
	else
		return false;
}
function modelpopup3(){

global $post;
$postid=$post->ID;
$link=get_permalink($postid);
global $user_ID
      ?>
      
<div class="modal fade" id="model2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><!--<span class="sr-only">Close</span>--></button>
        <h4 class="modal-title">Get contact info for this accommodation</h4>
      </div>
      <div class="modal-body">
      <div id="modelpopupbody">
      <input type="hidden" name="postid" id="postid" value="<?php echo $postid;?>" >
      <input type="hidden" name="userid" id="userid" value="<?php echo $user_ID;?>" >
      <p>You are out of credits.</p>
      <p>When you sign up you get 3 credits.<br />When you request contact info, you use 1 credit.<br />When you have less than 3 credits, we'll add 1 free credit every day.<br />read more about BBH Credits <a href="/bbh-credits" target="_blank">here</a></p>
      <p>To add credits and get the contact info for this property NOW:</p>
      <div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>
      <div class="modal-a-buttons">
        <!--<input type='button' name='share_message_to_facebook' value='Share message on your facebook timeline'>-->
      <input class="btn btn-primary" type='button' name='share_message_to_facebook' value='Share message on your facebook timeline' id="share_button">
      <?php echo do_shortcode('[paypal_button type="paynow" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/06/buy-credit-button.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]');?>
      </div>
      <!--<input type='button' name='buy_credits' value='Buy Credits'>-->
      </div>
      </div>
      <div class="modal-footer">
        
        <!--<a href="/bbh-credits/" target="blank" style="text-decoration:none;"><button type="button" class="btn btn-primary">How does this work?</button></a>-->
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


      <script>
      jQuery(document).ready(function($){
      <?php
      if ( is_user_logged_in() ) { 
         if(isset($_SESSION['popup']) && $_SESSION['popup'] =="1"){
      ?>
         jQuery("#model2").modal("show");       
         <?php $_SESSION['popup'] ="0"; ?>
      <?php
      }
      }
      ?>
   
         jQuery("#Tabs li a[href='#t3']").click(function(){
            jQuery("#model2").modal("show");       
         });
         
         

      });
         
      </script>
   <?php
}
function display_profile(){
   global $user_ID;
   $postlink=get_permalink( $posts->ID);
   $postlink=$postlink."#t3=true";
      $size ='60';
      $user = get_userdata($user_ID);
      $data= "<div id='fball-facebook-login1'><div style='margin-left:10px;padding:1px;'>"; 
      //$data.= '<a href="'.wp_logout_url(get_permalink()).'"><span>Log Out</span>';      
      $data.= '<a href="'.wp_logout_url($postlink).'"><span class="btn btn-primary">Log Out</span>';     
      $data.= '</a></div><div style="clear:both;"></div></div>'; 
      return $data;
}
/**
 * Function that getting api settings.
 */
  function facebookall_get_fb_contents($url) {
    $fball_settings = get_option('fball_settings');
    if ($fball_settings['connection_handler'] == 'curl') {
      $curl = curl_init();
     curl_setopt( $curl, CURLOPT_URL, $url );
     curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
     curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
      $response = curl_exec( $curl );
      curl_close( $curl );
      return $response;
    }
   else {
      $response = @file_get_contents($url);
      return $response;
   }
  }

/*
 * Function that checking username exist then adding index to it.
 */
  function facebookall_usernameexists($username) {
    $nameexists = true;
    $index = 0;
    $userName = $username;
    while($nameexists == true){
      if (username_exists($userName) != 0) {
        $index++;
        $userName = $username.$index;
      }
      else {
        $nameexists = false;
      }
    }
   return $userName;
  }

/*
 * Function getting user data from facebook.
 */
  function facebookall_get_fbuserprofile_data($fbuser_info) {
     $fbdata['id'] = (!empty($fbuser_info->id) ? $fbuser_info->id : '');
    $fbdata['first_name'] = (!empty($fbuser_info->first_name) ? $fbuser_info->first_name : '');
     $fbdata['last_name'] = (!empty($fbuser_info->last_name) ? $fbuser_info->last_name : '');
    $fbdata['name'] = (!empty($fbuser_info->name) ? $fbuser_info->name : '');
    $fbdata['gender'] = (!empty($fbuser_info->gender) ? $fbuser_info->gender : '');
    $fbdata['locale'] = (!empty($fbuser_info->locale) ? $fbuser_info->locale : '');
    $fbdata['timezone'] = (!empty($fbuser_info->timezone) ? $fbuser_info->timezone : '');
    $fbdata['email'] = (!empty($fbuser_info->email) ? $fbuser_info->email : '');
     $fbdata['thumbnail'] = "https://graph.facebook.com/" . $fbdata['id'] . "/picture";
     $fbdata['aboutme'] = (!empty($fbuser_info->bio) ? $fbuser_info->bio : "");
    $fbdata['website'] = (!empty( $fbuser_info->link) ? $fbuser_info->link : "");
    return $fbdata;
  }
  
/**
 * Get the userid.
 */
function facebookall_get_userid ($id) {
  global $wpdb;
  $find_id = "SELECT u.ID FROM " . $wpdb->usermeta . " AS um   INNER JOIN " . $wpdb->users . " AS u ON (um.user_id=u.ID)   WHERE um.meta_key = 'facebookall_user_id' AND um.meta_value=%s";
  return $wpdb->get_var ($wpdb->prepare ($find_id, $id));
}

/**
 * Redirect user after login.
 */
function facebookall_redirect_loggedin_user() {
  $fball_settings = get_option('fball_settings');
  switch ($fball_settings['redirect']) {
    case 'current':
      $redirect_to = facebookall_get_current_url();
     if($redirect_to == wp_login_url() OR $redirect_to == site_url().'/wp-login.php?action=register' OR $redirect_to == site_url().'/wp-login.php?loggedout=true'){ 
      $redirect_to = home_url();
     }
      break;
    case 'home':
      $redirect_to = home_url();
      break;
    case 'account':
      $redirect_to = admin_url();
      break;
    case 'custom':
     if (isset ($fball_settings['custom_url']) AND strlen (trim ($fball_settings ['custom_url'])) > 0) {        
        $redirect_to = trim ($fball_settings ['custom_url']);
      }
      break;
  }
  return $redirect_to;
}
