<?php

function facebookall_make_userlogin() {
global $new_user1;
  $fball_settings = get_option('fball_settings');
  if (isset($_GET['code']) AND !empty($_GET['code'])) {
    $code = $_GET['code'];
    parse_str(facebookall_get_fb_contents("https://graph.facebook.com/oauth/access_token?" . 'client_id=' . $fball_settings ['apikey'] . '&redirect_uri=' . urlencode(site_url()) .'&client_secret=' .  $fball_settings ['apisecret'] . '&code=' . urlencode($code)));
	if(empty($access_token)) {
	  parse_str(facebookall_get_fb_contents("https://graph.facebook.com/oauth/access_token?" . 'client_id=' . $fball_settings ['apikey'] . '&redirect_uri=' . urlencode(site_url().'/') .'&client_secret=' .  $fball_settings ['apisecret'] . '&code=' . urlencode($code)));
    }
	?>
	<script>
         window.opener.FbAll.parentRedirect({'action' : 'fball', 'fball_access_token' : '<?php echo $access_token?>'});
         window.close();
		 </script>
  <?php }
  if(!empty($_REQUEST['fball_access_token']) AND isset($_REQUEST['fball_redirect'])) {
    $fbuser_info = json_decode(facebookall_get_fb_contents("https://graph.facebook.com/me?access_token=".$_REQUEST['fball_access_token']));
    $fbdata = facebookall_get_fbuserprofile_data($fbuser_info);
	$_SESSION['popup'] ="1";
	//echo "<pre>";print_r($fbdata);exit;
    if (!empty($fbdata['email']) AND !empty($fbdata['id'])) {
      // Filter username form data.
      if(!empty($fbdata['name'])) {
        $username = $fbdata['name'];
      }
      else if (!empty($fbdata['first_name']) && !empty($fbdata['last_name'])) {
        $username = $fbdata['first_name'].$fbdata['last_name'];
      }
      else {
		$user_emailname = explode('@', $fbdata['email']);
        $username = $user_emailname[0];
      }
	  $user_login = sanitize_user($username, true);
	  $new_user = false;
	  //$new_user1 = 0;
      $user_id = facebookall_get_userid($fbdata['id']);
      if (!is_numeric ($user_id) AND empty($user_id)) {
        if (($user_id_tmp = email_exists ($fbdata['email'])) !== false) {
          $user_data = get_userdata ($user_id_tmp);
          if ($user_data !== false) {
            $user_id = $user_data->ID;
            $user_login = $user_data->user_login;
			if (!empty ($fbdata['gender'])) {
               update_user_meta ($user_id, 'facebook_user_gender', $fbdata['gender']);
             }
			 if (!empty ($fbdata['locale'])) {
               update_user_meta ($user_id, 'facebook_user_locale', $fbdata['locale']);
             }
			 if (!empty ($fbdata['timezone'])) {
               update_user_meta ($user_id, 'facebook_user_timezone', $fbdata['timezone']);
             }
            if (!isset ($fball_settings ['linkaccount']) OR $fball_settings ['linkaccount'] == 'link') {
              delete_metadata ('user', null, 'facebookall_user_id', $fbdata['id'], true);
              update_user_meta ($user_id, 'facebookall_user_id', $fbdata['id']);
              update_user_meta ($user_id, 'facebookall_user_email', $fbdata['email']);
			  update_user_meta ($user_id, 'new_user', "0");
			  
              if (!empty ($fbdata['thumbnail'])) {
                update_user_meta ($user_id, 'facebookall_user_thumbnail', $fbdata['thumbnail']);
              }
              wp_cache_delete ($user_id, 'users');
              wp_cache_delete ($user_login, 'userlogins');
            }
          }
        }
        else {
		  $new_user = true;
		  $new_user1 = 1;
          $user_login = facebookall_usernameexists($user_login);
          $user_password = wp_generate_password ();
		  $user_role = get_option('default_role');
          $user_data = array (
							'user_login' => $user_login,
							'display_name' => (!empty ($fbdata['name']) ? $fbdata['name'] : $user_login),
							'user_email' => $fbdata['email'],
							'first_name' => $fbdata['first_name'],
							'last_name' => $fbdata['last_name'],
							'user_url' => $fbdata['website'],
							'user_pass' => $user_password,
							'description' => $fbdata['aboutme'],
			                'role' => $user_role
						);
           $user_id = wp_insert_user ($user_data);
           if (is_numeric ($user_id)) {
		   
			add_user_meta($user_id, 'new_user', "1",true);
			$usercredit=get_option("credit");
             delete_metadata ('user', null, 'facebookall_user_id', $fbdata['id'], true);
             update_user_meta ($user_id, 'facebookall_user_id', $fbdata['id']);
             update_user_meta ($user_id, 'facebookall_user_email', $fbdata['email']);
			 
			// delete_metadata ('user', null, 'user_credit', 0, true);
			 add_user_meta ($user_id, 'user_credit', $usercredit,true);
             if (!empty ($fbdata['thumbnail'])) {
               update_user_meta ($user_id, 'facebookall_user_thumbnail', $fbdata['thumbnail']);
             }
			 if (!empty ($fbdata['gender'])) {
               update_user_meta ($user_id, 'facebook_user_gender', $fbdata['gender']);
             }
			 if (!empty ($fbdata['locale'])) {
               update_user_meta ($user_id, 'facebook_user_locale', $fbdata['locale']);
             }
			 if (!empty ($fbdata['timezone'])) {
               update_user_meta ($user_id, 'facebook_user_timezone', $fbdata['timezone']);
             }
             wp_cache_delete ($user_id, 'users');
             wp_cache_delete ($user_login, 'userlogins');
             do_action ('user_register', $user_id);
           }
         }
       }
       $user_data = get_userdata ($user_id);
       if ($user_data !== false) {
	     facebookall_post_user_wall($_REQUEST['fball_access_token'], $fbdata['id'], $new_user);
         wp_clear_auth_cookie ();
         wp_set_auth_cookie ($user_data->ID, true);
         do_action ('wp_login', $user_data->user_login, $user_data);
		 // Redirect user.
         if (!empty ($_GET['redirect_to'])) {
           $redirect_to = $_GET['redirect_to'];
           wp_safe_redirect ($redirect_to);
         }
         else {
           $redirect_to = facebookall_redirect_loggedin_user();
           wp_redirect ($redirect_to);
         }
         exit();
       }
     }
   }
 }
function after_login(){
	$user_ID = get_current_user_id(); 
	
	$new_user=get_user_meta($user_ID,"new_user",true);
	
	$STRING ="";
	$STRING.=display_profile();
	/*if($new_user ==1){ //new user
			$STRING.=modelpopup2();
	}*/
	//if($new_user == 0){
		$user_credit=get_user_meta($user_ID,"user_credit",true);
		if($user_credit > 0){
				$STRING.=modelpopup2();
		}
		if($user_credit == 0 || $user_credit == "")
		{
				$STRING.=modelpopup3();
		}
		
	//}
	 
	 return $STRING;
	
}
function modelpopup2(){
global $post;
$postid=$post->ID;
global $user_ID
		?>
		
<div class="modal fade" id="model2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><!--<span class="sr-only">Close</span>--></button>
        <h4 class="modal-title">Get contact info for this accommodation</h4>
      </div>
      <div class="modal-body">
		<div id="modelpopupbody">
      <?php if(!empty($user_ID)){
            $user_credit=get_user_meta($user_ID,"user_credit",true);
            $creditcontent=($user_credit == 1 ? "credit" : "credits");
         echo "<p><strong>You have <span id='user_credit'>".$user_credit."</span> ".$creditcontent."</strong>&nbsp;&nbsp;<a href='".site_url()."/bbh-credits/' target='_blank'><em>what's this?</em></a><br />".
         "Buy BBH credits: <a href='#'><strong>10x</strong>&nbsp;&nbsp;$10</a>,&nbsp;&nbsp;&nbsp;<a href='#'><strong>30x</strong>&nbsp;&nbsp;$25</a>,&nbsp;&nbsp;&nbsp;<a href='#'><strong>50x</strong>&nbsp;&nbsp;$100</a></p>";
         }
      ?>
      <input type="hidden" name="postid" id="postid" value="<?php echo $postid;?>" >
      <input type="hidden" name="userid" id="userid" value="<?php echo $user_ID;?>" >
		<!--<p>Do you prefer to contact the owner yourself (1 credit)? Or do you want to us to take care of virtual communications for you? (2 credits)? </p>-->
      <p>Most contact persons (either the owner or local agents) have limited English skills and the main feedback we get is that it’s difficult to communicate. If this isn’t a problem, we’ll send you the contact info we have (usually phone number).</p>
     <input class="btn btn-primary" type='button' name='contact_the_owner_myself' value='I will contact them myself  ( 1 credit )' id="contact_owner">
     <input class="btn btn-primary" type='button' name='share_message_to_facebook' value='Share to your Facebook timeline  ( FREE )' id="share_button">

      <p><br />We can also take care of all virtual communication (email/phone) for you. Other than this being convenient; as native Balinese we most likely can discuss/negotiate/notice questionable practices much better than you are able to yourself.</p>
      <input class="btn btn-primary" type='button' name='arrage_communications' value='Take care of communications for me  ( 2 credits )' id="arrange_communications">

      <p><br />Finally we can arrange a visit to the property and accompany you. We'll join you at a well known location and tell your driver where to find the property. At the property we'll take care of all necessary translations.</p>
      <input class="btn btn-primary" type='button' name='arrage_visit_to_the_house' value='Arrange for a visit and accompany me  ( 10 credits )' id="arrange_visit_house">

		<div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>
		<div class="modal-a-buttons">
		</div>
		</div>
      </div>
      <div class="modal-footer">
        
        <!--<a href="/bbh-credits/" target="blank" style="text-decoration:none;"><button type="button" class="btn btn-primary">How does this work?</button></a>-->
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


		<script>
		jQuery(document).ready(function($){
		<?php
		if ( is_user_logged_in() ) { 
			if(isset($_SESSION['popup']) && $_SESSION['popup'] =="1"){
		?>
			jQuery("#model2").modal("show");			
			<?php $_SESSION['popup'] ="0"; ?>
		<?php
		}
		}
		?>
	
	
			jQuery("#Tabs li a[href='#t3']").click(function(){
				jQuery("#model2").modal("show");			
			});
			
			

		});
			
		</script>
<?php

$STRING="";
/*		$STRING="";
		$STRING.="<input type='button' name='contact_the_owner_myself' value='I will Contact The Owner Myself'>";
		$STRING.="<input type='button' name='arrage_visit_to_the_house' value='Please arrange a visit to the house'>";
		return $STRING;*/
}
function modelpopup3(){

global $post;
$postid=$post->ID;
$link=get_permalink($postid);
global $user_ID
		?>
		
<div class="modal fade" id="model2">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><!--<span class="sr-only">Close</span>--></button>
        <h4 class="modal-title">Get contact info for this accommodation</h4>
      </div>
      <div class="modal-body">
		<div id="modelpopupbody">
		<input type="hidden" name="postid" id="postid" value="<?php echo $postid;?>" >
		<input type="hidden" name="userid" id="userid" value="<?php echo $user_ID;?>" >
		<p>You are out of credits.</p>
		<p>When you sign up you get 3 credits.<br />When you request contact info, you use 1 credit.<br />When you have less than 3 credits, we'll add 1 free credit every day.<br />read more about BBH Credits <a href="/bbh-credits" target="_blank">here</a></p>
		<p>To add credits and get the contact info for this property NOW:</p>
		<div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>
		<div class="modal-a-buttons">
        <!--<input type='button' name='share_message_to_facebook' value='Share message on your facebook timeline'>-->
		<?php //echo do_shortcode("[facebookall_share]");?>
		<input class="btn btn-primary" type='button' name='share_message_to_facebook' value='Share message on your facebook timeline' id="share_button">
		<?php //echo do_shortcode('[paypal_button type="paynow" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/06/buy-credit-button.png" thankyou_page_url="'.site_url().'/thankyou-buy-credit"]');?>
<?php echo do_shortcode('[paypal_button type="paynow" amount="10" name="10 BBH credits" btn_url="'.site_url().'/wp-content/uploads/2014/06/buy-credit-button.png" thankyou_page_url="'.$link.'#t3=true" returnmethod="1"]');?>
		</div>
		<!--<input type='button' name='buy_credits' value='Buy Credits'>-->
		</div>
      </div>
      <div class="modal-footer">
        
        <!--<a href="/bbh-credits/" target="blank" style="text-decoration:none;"><button type="button" class="btn btn-primary">How does this work?</button></a>-->
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


		<script>
		jQuery(document).ready(function($){
		<?php
		if ( is_user_logged_in() ) { 
			if(isset($_SESSION['popup']) && $_SESSION['popup'] =="1"){
		?>
			jQuery("#model2").modal("show");			
			<?php $_SESSION['popup'] ="0"; ?>
		<?php
		}
		}
		?>
	
			jQuery("#Tabs li a[href='#t3']").click(function(){
				jQuery("#model2").modal("show");			
			});
			
			

		});
			
		</script>
	<?php
}
function display_profile(){
	global $user_ID;
	$postlink=get_permalink( $posts->ID);
	$postlink=$postlink."#t3=true";
		$size ='60';
		$user = get_userdata($user_ID);
		$data= "<div id='fball-facebook-login'><div style='margin-left:10px;padding:1px;'>"; 
		//$data.= '<a href="'.wp_logout_url(get_permalink()).'"><span>Log Out</span>';		
		$data.= '<a href="'.wp_logout_url($postlink).'"><span>Log Out</span>';		
		$data.= '</a></div><div style="clear:both;"></div></div>'; 
		return $data;
}
/**
 * Function that getting api settings.
 */
  function facebookall_get_fb_contents($url) {
    $fball_settings = get_option('fball_settings');
    if ($fball_settings['connection_handler'] == 'curl') {
      $curl = curl_init();
	  curl_setopt( $curl, CURLOPT_URL, $url );
	  curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
	  curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
      $response = curl_exec( $curl );
      curl_close( $curl );
      return $response;
    }
	else {
	   $response = @file_get_contents($url);
	   return $response;
	}
  }

/*
 * Function that checking username exist then adding index to it.
 */
  function facebookall_usernameexists($username) {
    $nameexists = true;
    $index = 0;
    $userName = $username;
    while($nameexists == true){
      if (username_exists($userName) != 0) {
        $index++;
        $userName = $username.$index;
      }
      else {
        $nameexists = false;
      }
    }
	return $userName;
  }

/*
 * Function getting user data from facebook.
 */
  function facebookall_get_fbuserprofile_data($fbuser_info) {
     $fbdata['id'] = (!empty($fbuser_info->id) ? $fbuser_info->id : '');
	 $fbdata['first_name'] = (!empty($fbuser_info->first_name) ? $fbuser_info->first_name : '');
     $fbdata['last_name'] = (!empty($fbuser_info->last_name) ? $fbuser_info->last_name : '');
	 $fbdata['name'] = (!empty($fbuser_info->name) ? $fbuser_info->name : '');
	 $fbdata['gender'] = (!empty($fbuser_info->gender) ? $fbuser_info->gender : '');
	 $fbdata['locale'] = (!empty($fbuser_info->locale) ? $fbuser_info->locale : '');
	 $fbdata['timezone'] = (!empty($fbuser_info->timezone) ? $fbuser_info->timezone : '');
	 $fbdata['email'] = (!empty($fbuser_info->email) ? $fbuser_info->email : '');
     $fbdata['thumbnail'] = "https://graph.facebook.com/" . $fbdata['id'] . "/picture";
     $fbdata['aboutme'] = (!empty($fbuser_info->bio) ? $fbuser_info->bio : "");
	 $fbdata['website'] = (!empty( $fbuser_info->link) ? $fbuser_info->link : "");
	 return $fbdata;
  }
  
/**
 * Get the userid.
 */
function facebookall_get_userid ($id) {
  global $wpdb;
  $find_id = "SELECT u.ID FROM " . $wpdb->usermeta . " AS um	INNER JOIN " . $wpdb->users . " AS u ON (um.user_id=u.ID)	WHERE um.meta_key = 'facebookall_user_id' AND um.meta_value=%s";
  return $wpdb->get_var ($wpdb->prepare ($find_id, $id));
}

/**
 * Redirect user after login.
 */
function facebookall_redirect_loggedin_user() {
  $fball_settings = get_option('fball_settings');
  switch ($fball_settings['redirect']) {
    case 'current':
      $redirect_to = facebookall_get_current_url();
	  if($redirect_to == wp_login_url() OR $redirect_to == site_url().'/wp-login.php?action=register' OR $redirect_to == site_url().'/wp-login.php?loggedout=true'){ 
		$redirect_to = home_url();
	  }
      break;
    case 'home':
      $redirect_to = home_url();
      break;
    case 'account':
      $redirect_to = admin_url();
      break;
    case 'custom':
	  if (isset ($fball_settings['custom_url']) AND strlen (trim ($fball_settings ['custom_url'])) > 0) {        
        $redirect_to = trim ($fball_settings ['custom_url']);
      }
      break;
  }
  return $redirect_to;
}
