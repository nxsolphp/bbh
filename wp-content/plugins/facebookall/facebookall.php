<?php
/*
Plugin Name: Facebook All
Plugin URI: http://www.sourceaddons.com/
Description: Allow your visitors to <strong>comment, login, register and share with facebook and google </strong> also post on their facebook wall.
Version: 1.4
Author: sourceaddons
Author URI: http://www.sourceaddons.com/
License: GPL2
*/

define ('FACEBOOKALL_PLUGIN_URL', plugins_url () . '/' . basename (dirname (__FILE__)));
require_once(dirname (__FILE__) . '/facebookall_admin.php');
require_once(dirname (__FILE__) . '/helpers/facebookall_view.php');
require_once(dirname (__FILE__) . '/helpers/facebookall_widgets.php');
require_once(dirname (__FILE__) . '/helpers/facebookall_process.php');
require_once(dirname (__FILE__) . '/helpers/facebookall_toolbox.php');
require_once(dirname (__FILE__) . '/helpers/facebookall_share.php');

/**
 * Add js to front side.
 */
function facebookall_front_scripts() {
  $fball_settings = get_option('fball_settings');
  
  if ($fball_settings['share_pin'] == '1') {
    wp_register_script('pinjs', 'http://assets.pinterest.com/js/pinit.js', false, '1.4.2');
    wp_enqueue_script('pinjs');
  }
  if ($fball_settings['share_linkedin'] == '1') {
    wp_register_script('linkedinjs', 'http://platform.linkedin.com/in.js', false, '1.4.2');
    wp_enqueue_script('linkedinjs');
  }
  if ($fball_settings['share_twitter'] == '1') {
    wp_register_script('twitterjs', 'http://platform.twitter.com/widgets.js', false, '1.4.2');
    wp_enqueue_script('twitterjs');
  }
  if ($fball_settings['share_gplus'] == '1') {
    wp_register_script('gplusjs', 'https://apis.google.com/js/plusone.js', false, '1.4.2');
    wp_enqueue_script('gplusjs');
  }
  if( !wp_script_is( 'connect_js', 'registered' ) ) {
    wp_register_script('connect_js', plugins_url('assets/js/fball_connect.js', __FILE__), false, '1.0.0');
  }
  wp_print_scripts( "connect_js" );
 
  
 
}

function facebookall_fbmlsetup() {
  $fball_settings = get_option('fball_settings');
  if (!empty($fball_settings['apikey']) || !empty($fball_settings['recbar_appid']) || !empty($fball_settings['comment_appid'])) {
    $appid = (!empty($fball_settings['apikey']) ? $fball_settings['apikey'] : $fball_settings['comment_appid']);
    $appid = (!empty($appid) ? $appid : $fball_settings['recbar_appid']);
  }?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=<?php echo $appid; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php }
add_action('wp_head', 'facebookall_fbmlsetup', 100);
add_action('wp_footer','fbshare');
function fbshare(){
	global $wpdb,$post,$user_ID;
	$template_directory= get_template_directory_uri();
	?>
        <script src="<?php echo site_url();?>/wp-content/themes/RT/framework/js/run_prettify.js"></script>
<link href="<?php echo site_url();?>/wp-content/themes/RT/framework/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="<?php echo site_url();?>/wp-content/themes/RT/framework/js/bootstrap-dialog.min.js"></script>
	<script>
	jQuery(document).ready(function(){
	jQuery('#share_button').click(function(e){
	var renting_this_property_from=jQuery("#renting_this_property_from").val();
		var rent_arr=renting_this_property_from.split("/");
		//alert(rent_arr[0]);
		var formatdate=rent_arr[2]+"-"+rent_arr[0]+"-"+rent_arr[1];
		//alert(formatdate);
		var my_home_country_is=jQuery("#my_home_country_is").val();
		var error=checkrentpropertyandhomecity(renting_this_property_from,my_home_country_is);
		if(!error){return false;}
		var start = new Date(formatdate);
		var end = new Date();
		var diff = new Date(start - end);
		var days = diff/1000/60/60/24;
		var diff = Math.ceil(days);
		//alert(diff);
		if(diff >= 30)
		{
			 BootstrapDialog.show({
            title: 'Alert Message',
            message: "We noticed you intend to rent this property in a while. Owners usually want to rent their properties as soon as possible and as long as possible. We'll gladly send you the contact info for this property, however chances are low this will work out. Do you still want us to send you the contact info for this property?",
            buttons: [{
                label: 'Yes',
                action: function(dialog) {
				jQuery( ".bootstrap-dialog-close-button .close" ).trigger( "click" );
		jQuery("#loading").show(); 
			
		e.preventDefault(); 
		FB.ui({
		method: 'feed',	
		/*link: '<?php echo get_permalink();?>',*/
		link:'http://balibudgethousing.com/',
		caption: 'balibudgethousing.com',
		message:'This is testing message',
		},  
		function(response) {
	    	if (response && response.post_id) {
	    	jQuery("#share_button").attr('disabled','disabled');
		jQuery("#share_button").before('<span class="tooltip-wrapper" data-toggle="tooltip" data-placement="top" title="You can share a message to Facebook once per day. Please use another option or wait until tomorrow."></span>');	
		jQuery('.tooltip-wrapper').tooltip("show");
				var msg="";
				jQuery.ajax({	
				type: 'POST',
				url: "<?php echo $template_directory;?>/loadmodel.php",
				data: 'action=share_message_send_contact&&postid=<?php echo $post->ID;?>&&userid=<?php echo $user_ID;?>&renting_this_property_from='+renting_this_property_from+'&my_home_country_is='+my_home_country_is,
				cache: false,
				success: function(response) {
					jQuery("#loading").hide(); 
					var d = jQuery.parseJSON(response);
					msg +=d.msg;
					/*msg +="<br /><br />";*/
					msg +="<p>Didn't get our email? Please check your <input type='hidden' id='postid' name='postid' value='<?php echo $post->ID;?>' /><a href='<?php echo site_url();?>/spam' target='_blank'>spam folder </a> or <a href='javascript:;' onclick='resend_mail_to_user1();'>click here</a> and we'll resend the email now.</p>";
					/*msg +="<input type='button' class='btn btn-primary' name='share_property_to_facebook' onclick='share_property_to_facebook();' value='Share property on Facebook' />";*/
					jQuery("#responsebody").html(msg+'<div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>');
					jQuery("#user_credit").html(d.remain_credit);
					jQuery("#modelpopupbody").hide(); 
				}
			});
			
			
			/*	var msg='<div class="alert alert-success" role="alert">The message was shared on your Facebook timeline.</div>';
				jQuery("#modelpopupbody").html(msg);
			*/
			} else {
				jQuery("#loading").hide();
				var msg='<div class="alert alert-danger" role="alert">The message wasn\'t shared on your Facebook timeline.</div>';
				msg +="<input type='button' class='btn btn-primary' name='fb_share_try_again' onclick='fb_share_try_again();' value='Try Again' />";
				jQuery("#responsebody").html(msg);
				jQuery("#modelpopupbody").hide(); 
			}   
		});
                }
            }, {
                label: 'No',
                action: function(dialog) {
                   jQuery( ".close" ).trigger( "click" );
				return false;
                }
            }]
        });
			
		}
		else
		{
		var error=checkrentpropertyandhomecity(renting_this_property_from,my_home_country_is);
		if(!error){return false;}
		jQuery("#loading").show(); 
			
		e.preventDefault(); 
		FB.ui({
		method: 'feed',	
		/*link: '<?php echo get_permalink();?>',*/
		link:'http://balibudgethousing.com/',
		caption: 'balibudgethousing.com',
		message:'This is testing message',
		},  
		function(response) {
		
	    	if (response && response.post_id) {
	    	jQuery("#share_button").attr('disabled','disabled');
		jQuery("#share_button").before('<span class="tooltip-wrapper" data-toggle="tooltip" data-placement="top" title="You can share a message to Facebook once per day. Please use another option or wait until tomorrow."></span>');	
		jQuery('.tooltip-wrapper').tooltip("show");
				var msg="";
				jQuery.ajax({	
				type: 'POST',
				url: "<?php echo $template_directory;?>/loadmodel.php",
				data: 'action=share_message_send_contact&&postid=<?php echo $post->ID;?>&&userid=<?php echo $user_ID;?>&renting_this_property_from='+renting_this_property_from+'&my_home_country_is='+my_home_country_is,
				cache: false,
				success: function(response) {
					jQuery("#loading").hide(); 
					var d = jQuery.parseJSON(response);
					msg +=d.msg;
					/*msg +="<br /><br />";*/
					msg +="<p>Didn't get our email? Please check your <input type='hidden' id='postid' name='postid' value='<?php echo $post->ID;?>' /><a href='<?php echo site_url();?>/spam' target='_blank'>spam folder </a> or <a href='javascript:;' onclick='resend_mail_to_user1();'>click here</a> and we'll resend the email now.</p>";
					/*msg +="<input type='button' class='btn btn-primary' name='share_property_to_facebook' onclick='share_property_to_facebook();' value='Share property on Facebook' />";*/
					jQuery("#responsebody").html(msg+'<div id="loading" style="display:none;"><img src="<?php echo site_url();?>/wp-content/uploads/2014/08/loading.gif"></div>');
					jQuery("#user_credit").html(d.remain_credit);
					jQuery("#modelpopupbody").hide(); 
				}
			});
			
			
			/*	var msg='<div class="alert alert-success" role="alert">The message was shared on your Facebook timeline.</div>';
				jQuery("#modelpopupbody").html(msg);
			*/
			} else {
				jQuery("#loading").hide();
				var msg='<div class="alert alert-danger" role="alert">The message wasn\'t shared on your Facebook timeline.</div>';
				msg +="<input type='button' class='btn btn-primary' name='fb_share_try_again' onclick='fb_share_try_again();' value='Try Again' />";
				jQuery("#responsebody").html(msg);
				jQuery("#modelpopupbody").hide(); 
			}   
		});
		}
		}); 
		});
		function share_property_to_facebook(){
			FB.ui({
		method: 'feed',	
		link: '<?php echo get_permalink();?>',
		/*link:'http://balibudgethousing.com/',*/
		caption: 'balibudgethousing.com',
		message:'This is testing message',
		},  
		function(response) {
		
	    	if (response && response.post_id) {
				jQuery("#responsebody").html('<div class="alert alert-success" role="alert">Listing was shared on your Facebook timeline</div>');
				jQuery("#modelpopupbody").hide(); 
				}else {
				jQuery("#loading").hide();
				var msg='<div class="alert alert-danger" role="alert">The message wasn\'t shared on your Facebook timeline.</div>';
				
				jQuery("#responsebody").html(msg);
				jQuery("#modelpopupbody").hide(); 
			} 
				
			});
			
			
			
		
		}
		function fb_share_try_again(){
			jQuery("#Tabs li a[href='#t3']").click();
		}
		</script><?php 
}
add_action ('login_head', 'facebookall_front_scripts');
add_action ('wp_head', 'facebookall_front_scripts');

/**
 * Add front end style.
 */
function facebookall_add_fbbutton_style() {
  wp_register_style('facebookall-button-style', plugins_url('assets/css/fball_fbbutton.css', __FILE__));
  wp_enqueue_style( 'facebookall-button-style' );
}
add_action( 'wp_enqueue_scripts', 'facebookall_add_fbbutton_style' );
add_action ('login_head', 'facebookall_add_fbbutton_style');
/**
 * Add administration area links
 **/
function facebookall_admin_menu () {
  $page = add_menu_page ('Facebook All ', 'Facebook All', 'manage_options', 'facebookall', 'facebookall_admin_settings',site_url()."/bbhpluginicon.png",85);
  add_action('admin_print_scripts-' . $page, 'facebookall_options_page_scripts');
  add_action('admin_print_styles-' . $page, 'facebookall_options_page_style');
}
add_action ('admin_menu', 'facebookall_admin_menu');


/**
 * Set default settings on plugin activation.
 */
function facebookall_default_options() {
   global $fball_settings;
    add_option( 'fball_settings',
	    array( 'login_title' => 'Or',
		       'fbicon_text' => 'Login with Facebook',
		       'loginpage' => '1',
               'registerpage' => '1',
               'commentpage' => '1',
               'fanbox_pageurl' => 'http://www.facebook.com/pages/Source-addons/162763307197548',
               'fanbox_width' => '200',
               'fanbox_height' => '200',
               'facepile_pageurl' => 'http://www.facebook.com/pages/Source-addons/162763307197548',
               'facepile_width' => '200',
               'facepile_numrows' => '2',
        ));
}
register_activation_hook(__FILE__, 'facebookall_default_options');

/**
 * Initialise
 */
add_action ('init', 'facebookall_make_userlogin', 9);


